package Controller;

//Imports
import java.util.ArrayList;

import Model.*;


public class ControladorAtleta {

	//Definindo Array
	static ArrayList <Futebolista> listaFut;
	static ArrayList <Tenista> listaTenis;
	
	
	public ControladorAtleta(){
		listaFut = new ArrayList<Futebolista>();
		listaTenis = new ArrayList<Tenista>();
	}
	

	public void adicionarF(Futebolista umFutebolista){
		System.out.println("Adicionando Futebolista...");
		listaFut.add(umFutebolista);
		System.out.println("Adicionado com Sucesso!");
	}
	
	public void adicionarT(Tenista umTenista){
		System.out.println("Adicionando Tenista...");
		listaTenis.add(umTenista);
		System.out.println("Adicionado com Sucesso!");
	}
	
	
	public void removerT(Tenista umTenista){
		if (umTenista == null)
		{ 
			System.out.println("Nao encontrado.");
			
		}else
		{
		System.out.println("Removendo Tenista...");
		listaTenis.remove(umTenista);
		System.out.println("Removido com Sucesso!");
		}
	}
	
	public void removerF(Futebolista umFutebolista){
		if (umFutebolista == null)
		{ 
			System.out.println("Nao encontrado.");
			
		}else
		{
		System.out.println("Removendo Futebolista...");
		listaFut.remove(umFutebolista);
		System.out.println("Removido com Sucesso!"); 
		} 
	}

	
	public Futebolista pesquisarF(String umNome){
		for(Futebolista umFutebolista : listaFut){
			if(umFutebolista.getNome().equalsIgnoreCase(umNome))
			return umFutebolista;
		}
		return null;
	}
	
	public Tenista pesquisarT(String umNome){
		for(Tenista umTenista : listaTenis){
			if(umTenista.getNome().equalsIgnoreCase(umNome))
				return umTenista;
		}
		return null;
	}
	
	
	public void exibirF(){
		if(listaFut.size()>0){
			System.out.println("Futebolistas Cadastrados:");
		for(Futebolista umFutebolista : listaFut){
			System.out.println(umFutebolista.getNome());
		} 
		} 
	}
	
	public void exibirT(){
		if(listaTenis.size()>0){
			System.out.println("Tenistas Cadastrados:");
		for(Tenista umTenista : listaTenis){
			System.out.println(umTenista.getNome());
		} 
		} 
	}
	
		public void caractF(Futebolista umFutebolista){
		System.out.println("  Futebolista");	
		System.out.println("Nome: " + umFutebolista.getNome());
		System.out.println("Idade: " + umFutebolista.getIdade());
		System.out.println("Peso: " + umFutebolista.getPeso());
		Endereco oEndereco =  umFutebolista.getEndereco();
		System.out.println("Estado: " + oEndereco.getEstado());
		System.out.println("Estado: " + oEndereco.getCidade());
		System.out.println("Logradouro: " + oEndereco.getLogradouro());
		System.out.println("Numero: " + oEndereco.getNumero());
		}
		
		public void caractT(Tenista umTenista){
		System.out.println("  Tenista");	
		System.out.println("Nome: " + umTenista.getNome());
		System.out.println("Idade: " + umTenista.getIdade());
		System.out.println("Peso: " + umTenista.getPeso());
		Endereco oEndereco =  umTenista.getEndereco();
		System.out.println("Estado: " + oEndereco.getEstado());
		System.out.println("Estado: " + oEndereco.getCidade());
		System.out.println("Logradouro: " + oEndereco.getLogradouro());
		System.out.println("Numero: " + oEndereco.getNumero());
		}
}