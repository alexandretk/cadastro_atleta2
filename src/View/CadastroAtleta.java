package View;

import Model.*;
import Controller.*;
import java.util.Scanner;

//Classe que faz a "ligacao" das outras com o usuario... pertence a VIEW
public class CadastroAtleta {


	public static void main(String[] args) {
		// Opcao inicial para o loop
		int opcao = -1;
		int opcao2 = -1;
		
		// Classe para ler da tela
		Scanner read = new Scanner(System.in);
		//
//		Atleta umAtleta = null;
		Futebolista umFutebolista = null;
		Tenista umTenista = null;
		
		Endereco umEndereco = null;
		
		ControladorAtleta umControlador = new ControladorAtleta();
		Menu menu = new Menu();

		while(opcao != 0){
			menu.menuInicial();
			opcao = read.nextInt();
			read.nextLine(); //Limpar buffer
			
			switch(opcao){
			
			case 1:
				
				// Adicionar Atleta
				while(opcao2 != 0) {
					
					System.out.println(" \nDigite:"
							+ "\n 1 - para cadastrar um Futebolista "
							+ "\n 2 - para cadastrar um Tenista"
							+ "\n 0 - para sair da tela de cadastro");
					opcao2 = read.nextInt();
					read.nextLine(); //Limpar buffer
					
					switch(opcao2){
					case 0:
						break;
						
					case 1:
						
				System.out.println("Informe o Nome do atleta a ser cadastrado:");
				String nome= read.nextLine();
				
				System.out.println("Informe a posicao do jogador em campo:");
				String posicao= read.nextLine();
				
				umFutebolista = new Futebolista(nome, posicao);
				
				System.out.println("Informe a Idade desse atleta:");
				umFutebolista.setIdade(read.next());
				
				System.out.println("Informe o Peso desse atleta:");
				umFutebolista.setPeso(read.next());
			
				boolean ok = false;
				do {
				System.out.println("O jogador e destro ou canhoto?"
						+ "\n d - para destro;"
						+ "\n c - para canhoto.");
				String CouD = read.next();
				if(CouD.equalsIgnoreCase("d")){
					umFutebolista.setDestro(true);
					ok=true;
				} else {
				if(CouD.equalsIgnoreCase("c")){
					umFutebolista.setDestro(false);
					ok=true;
				}
				}
				} while(!ok);
				
				// Instanciar Endereco
				
				System.out.println("---ENDERECO---");
				
				System.out.println("Informe o CEP:");
				String cep = read.next();
				umEndereco = new Endereco(cep);

				
				System.out.println("Informe o Estado:");
				umEndereco.setEstado(read.next());
				
				System.out.println("Informe a Cidade:");
				umEndereco.setCidade(read.next());
				
				
				System.out.println("Informe o Logradouro:");
				umEndereco.setLogradouro(read.next());
				
				System.out.println("Informe o Numero:");
				umEndereco.setNumero(read.next());
			
				// Passando Endereco para Atleta.
				umFutebolista.setEndereco(umEndereco);
				umControlador.adicionarF(umFutebolista);
				opcao2=0;
				break;
				
					case 2:
					
						System.out.println("Informe o Nome do atleta a ser cadastrado:");
						nome= read.nextLine();
						
						System.out.println("Informe a forca do saque ( 1 a 5):");
						int forca= read.nextInt();
						
						umTenista = new Tenista(nome, forca);
						
						System.out.println("Informe a Idade desse atleta:");
						umTenista.setIdade(read.next());
						
						System.out.println("Informe o Peso desse atleta:");
						umTenista.setPeso(read.next());
					
						boolean ok2 = false;
						do{
						System.out.println("O jogador e destro ou canhoto?"
								+ "\n d - para destro;"
								+ "\n c - para canhoto.");
						String CouD = read.next();
						if(CouD.equalsIgnoreCase("d")){
							umTenista.setEstilo('D'); 
							ok2=true;
						} else {
						if(CouD.equalsIgnoreCase("c")){
							umTenista.setEstilo('C');
							ok2=true;
						}
						}
						}while(!ok2);
						
					
							
						// Instanciar Endereco
						
						System.out.println("---ENDERECO---");
						
						System.out.println("Informe o CEP:");
					    cep = read.next();
						umEndereco = new Endereco(cep);

						
						System.out.println("Informe o Estado:");
						umEndereco.setEstado(read.next());
						
						System.out.println("Informe a Cidade:");
						umEndereco.setCidade(read.next());
						
						
						System.out.println("Informe o Logradouro:");
						umEndereco.setLogradouro(read.next());
						
						System.out.println("Informe o Numero:");
						umEndereco.setNumero(read.next());
					
						// Passando Endereco para Atleta.
						umTenista.setEndereco(umEndereco);
						umControlador.adicionarT(umTenista);	
						opcao2=0;
			break;

					default :
					System.out.println("\nDigite novamente. ");
					}
				}
			if(opcao2==0)
			{
				opcao2=-1;
				break;
			}
				
			case 2:
				//Remover Atletas
				umControlador.exibirF();
				umControlador.exibirT();
				System.out.println("\nDigite o Nome do Atleta a ser deletado: ");
				String nome= read.nextLine();
				umFutebolista = umControlador.pesquisarF(nome);
				umTenista = umControlador.pesquisarT(nome);
				if ((umFutebolista == null) && (umTenista == null)) {
						System.out.println("Nao encontrado");
						} else{
				if ((umFutebolista != null) && (umTenista != null)) 
					{
					umControlador.removerF(umFutebolista);
					umControlador.removerT(umTenista);
					System.out.println("Atleta Futebolista e Tenista Deletado");
					} else{
				if (umTenista == null)
				{
					umControlador.removerF(umFutebolista);
					System.out.println("Futebolista Deletado");
				} else {
					umControlador.removerT(umTenista);
					System.out.println("Tenista Deletado");
				}
					}
						}
			//	umControlador.remover(umAtleta);
				
			break;
			
			case 3:
				// Exibir Atletas
				if((umFutebolista==null)&&(umTenista==null)){
					System.out.println("Nenuhum atleta encontrado.");
				}else{
				umControlador.exibirT();
				umControlador.exibirF();
				System.out.println("   Para ver os dados de cada um acesse Pesquisa de Atletas.");
				}

			break;
			
			case 4:
				// Listar Atletas
				System.out.println("\t Pesquisa de Atletas");

				System.out.println("\nDigite o Nome do Atleta a ser pesquisado: ");
				nome= read.nextLine();
				Futebolista oFut = umControlador.pesquisarF(nome);//umControlador.pesquisarAtleta(oNome);
				Tenista oTenista = umControlador.pesquisarT(nome);
				if ((umFutebolista == null) && (umTenista == null)) {
					System.out.println("Nao encontrado");
					} else{
			if ((umFutebolista != null) && (umTenista != null)) 
				{
				System.out.println("Futebolista e Tenista com o mesmo nome");
				umControlador.caractF(oFut);
	            umControlador.caractT(oTenista);
				} else{
			if (umTenista != null)
			{
	            umControlador.caractT(oTenista);
			} else {
				umControlador.caractF(oFut);
			}
				}
					}
				break;			
			
			case 0:
				System.out.println("Fechando programa...");
				System.out.println("Programa Fechado Com Sucesso!");
				System.exit(0);
				
			break;
			
			default:
				System.out.println("Opção Invalida! Digite Outra: ");
			break;
			
			}
		}
	}

}