package View;

public class Menu {

	public void menuInicial(){
		
		System.out.println(
				"\n  Cadastro Atleta \n"
				+ "1 - Adicionar novo Atleta\n"
				+ "2 - Remover Atleta\n"
				+ "3 - Listar Atleta\n"
				+ "4 - Pesquisar Atleta\n" 
				+ "0 - Fechar o Programa");
	}
}